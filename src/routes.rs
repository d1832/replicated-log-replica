use actix_web::{get, web, Error, HttpRequest, HttpResponse, Responder};
use futures::future::{ready, Ready};
use std::sync::{Arc, Mutex};

use super::messages::MessagesList;
use super::socket_client::SocketClient;

pub struct AppState {
    pub messages_list: Arc<Mutex<MessagesList>>,
    pub socket_client: Arc<Mutex<SocketClient>>,
}

impl Responder for MessagesList {
    type Error = Error;
    type Future = Ready<Result<HttpResponse, Error>>;

    fn respond_to(self, _req: &HttpRequest) -> Self::Future {
        let body = serde_json::to_string(&self).unwrap();

        ready(Ok(HttpResponse::Ok()
            .content_type("application/json")
            .body(body)))
    }
}

#[get("/messages")]
async fn messages_list(data: web::Data<AppState>) -> impl Responder {
    let messages = data.messages_list.lock().unwrap();

    messages.clone()
}
