#[macro_use]
extern crate log;

mod config;
mod messages;
mod routes;
mod socket_client;

use std::sync::{Arc, Mutex};
use std::thread;

use actix_web::{web, App, HttpServer};
use config::Config;
use messages::{Message, MessagesList};
use routes::{messages_list, AppState};
use socket_client::SocketClient;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    dotenv::dotenv().ok();
    env_logger::init();

    let config = Config::new();
    let messages = Arc::new(Mutex::new(MessagesList::default()));
    let messages_for_state = Arc::clone(&messages);
    let messages_for_socket = Arc::clone(&messages);

    let client = Arc::new(Mutex::new(SocketClient::new(config.socket_address)));
    let socket_client_for_state = Arc::clone(&client);
    let socket_client_for_listener = Arc::clone(&client);

    let app_state = web::Data::new(AppState {
        socket_client: socket_client_for_state,
        messages_list: messages_for_state,
    });

    thread::spawn(move || {
        let client = socket_client_for_listener.lock().unwrap();

        client.receive_message(move |message| {
            let mut log_messages_list = messages_for_socket.lock().unwrap();

            let message_to_add = Message::new(message.id, message.text.to_string());

            log_messages_list.add_message(message_to_add);

            //TODO: Send reply after sendig message
        })
    });

    let address = format!("{}:{}", config.host, config.port);
    info!("Server listen on {}", address);
    HttpServer::new(move || {
        App::new()
            .app_data(app_state.clone())
            .service(messages_list)
    })
    .bind(address)?
    .run()
    .await
}
