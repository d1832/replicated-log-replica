use rand::prelude::*;

pub struct ServiceSupport {
    socket: zmq::Socket,
}

impl ServiceSupport {
    pub fn new(context: zmq::Context, socket_address: &str) -> Self {
        let socket = context.socket(zmq::PUSH).unwrap();

        socket.connect(socket_address).unwrap();

        Self { socket }
    }

    pub fn register_node(&self) {
        let id = self.generate_node_id();
        let message = format!("node_{}", id);
        let result = self.socket.send(&message, 0);

        match result {
            Ok(_) => info!("Message send success"),
            Err(err) => error!("Error: {}", err),
        }
    }

    fn generate_node_id(&self) -> i32 {
        let mut rng = thread_rng();
        rng.gen_range(0..10_000)
    }
}
