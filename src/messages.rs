use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct Message {
    pub id: i32,
    pub text: String,
}

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct MessagesList {
    messages: Vec<Message>,
}

impl Message {
    pub fn new(id: i32, text: String) -> Self {
        Self { id, text }
    }
}

impl MessagesList {
    pub fn add_message(&mut self, message: Message) {
        self.messages.push(message);
    }
}
