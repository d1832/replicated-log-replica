use super::messages::Message;
use std::sync::Arc;
use zmq;

pub struct SocketClient {
    subscriber: zmq::Socket,
}

impl SocketClient {
    pub fn new(socket_address: String) -> Self {
        let ctx = zmq::Context::new();
        let subscriber = ctx.socket(zmq::REP).unwrap();

        let subscription_result = subscriber.connect(&socket_address);

        match subscription_result {
            Ok(connection) => info!("Connected app: {:?}", connection),
            Err(err) => {
                error!("Error connecting to socket {:?}", err);

                subscriber.connect(&socket_address).unwrap();
                info!("Socket connected");
            }
        }

        Self { subscriber }
    }

    pub fn receive_message<F>(&self, callback: F)
    where
        F: Fn(&Message),
    {
        loop {
            let result = self.subscriber.recv_string(0).unwrap();

            match result {
                Ok(serialized_message) => {
                    if serialized_message.len() != 0 {
                        let message: Message = serde_json::from_str(&serialized_message).unwrap();

                        callback(&message);

                        println!("Message received {:?}", &message);
                        self.subscriber.send("Message received", 0).unwrap();
                    }
                }
                Err(err) => error!("Error {:?}", String::from_utf8_lossy(&err)),
            }
        }
    }
}
